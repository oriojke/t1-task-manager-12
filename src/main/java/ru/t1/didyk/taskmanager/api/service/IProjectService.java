package ru.t1.didyk.taskmanager.api.service;

import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name, String description);

    List<Project> findAll();

    void clear();

    Project add(Project project);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
