package ru.t1.didyk.taskmanager.api.service;

import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    List<Task> findAll();

    void clear();

    void add(Task task);

    Task create(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
